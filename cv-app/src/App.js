import "./App.scss";
import InnerPage from "./pages/Inner/InnerPage";
import HomePage from "./pages/Home/HomePage";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/inner" element={<InnerPage />} />
      </Routes>
    </Router>
  );
}

export default App;
