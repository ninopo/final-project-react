import {
  Box,
  TimeLine,
  Panel,
  Expertise,
  Feedback,
  Address,
  Portfolio,
  Skills,
} from "../../components";
import { animateScroll as scroll } from "react-scroll";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronUp, faBars } from "@fortawesome/free-solid-svg-icons";
import { useState, useEffect } from "react";
import useWindowSIze from "../../hooks/windowSize";
import "./InnerPage.scss";
import { expertiseArray, aboutText, feedbackArray } from "../../utils/data";

export default function InnerPage() {
  const scrollToTop = () => {
    scroll.scrollToTop({ duration: 500 });
  };

  const [width] = useWindowSIze();
  const [isPanelMinimized, setIsPanelMinimized] = useState(false);
  const [isPanelHidden, setIsPanelHidden] = useState(false);

  useEffect(() => {
    if (width > 900) {
      setIsPanelMinimized(false);
      setIsPanelHidden(false);
    } else {
      setIsPanelMinimized(true);
    }
  }, [width]);

  const handleMinimizeButtonClick = () => {
    if (width > 900) {
      setIsPanelMinimized(!isPanelMinimized);
    } else {
      setIsPanelHidden(!isPanelHidden);
    }
  };

  return (
    <div className="inner-wrapper">
      {!isPanelHidden && (
        <div
          className={`panel-container ${isPanelMinimized ? "minimized" : ""}`}
        >
          <Panel isMinimized={isPanelMinimized} />
        </div>
      )}

      <div
        className={`inner-container ${isPanelMinimized ? "minimized" : ""} ${
          isPanelHidden ? "hidden" : ""
        }`}
      >
        <div className="box-container" id="about-me">
          <Box title="About me" content={aboutText} />
        </div>

        <div className="box-container" id="education">
          <Box title="Education" content={<TimeLine />} />
        </div>

        <div className="box-container" id="experience">
          <Box
            title="Experience"
            content={<Expertise data={expertiseArray} />}
          />
        </div>

        <div className="box-container" id="skills">
          <Box title="Skills" content={<Skills />} />
        </div>

        <div className="box-container" id="portfolio">
          <Box title="Portfolio" content={<Portfolio />} />
        </div>

        <div className="box-container" id="contacts">
          <Box title="Contacts" content={<Address />} />
        </div>

        <div className="box-container" id="feedbacks">
          <Box title="Feedbacks" content={<Feedback data={feedbackArray} />} />
        </div>
      </div>

      <button
        className={`minimize-panel ${isPanelMinimized ? "minimized" : ""} ${
          isPanelHidden ? "hidden" : ""
        }`}
        onClick={handleMinimizeButtonClick}
      >
        <FontAwesomeIcon icon={faBars} />
      </button>

      <button className="scroll-to-top-btn" onClick={scrollToTop}>
        <FontAwesomeIcon icon={faChevronUp} />
      </button>
    </div>
  );
}
