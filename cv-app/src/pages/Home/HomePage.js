import "./HomePage.scss";
import PhotoBox from "../../components/PhotoBox/PhotoBox";
import Button from "../../components/Button/Button";

export default function HomePage() {
  return (
    <div className="home-container">
      <div className="content">
        <PhotoBox
          name="John Doe"
          title="Programmer. Creative. Innovator"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque"
          avatar="/assets/images/avatar.png"
          fontSize="large"
        />

        <Button text="Know more" link="/inner" />
      </div>
    </div>
  );
}
