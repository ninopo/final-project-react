import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const fetchSkills = createAsyncThunk("skills/fetchSkills", async () => {
  const response = await fetch("/api/skills");
  return response.json();
});

export const postSkill = createAsyncThunk(
  "skills/postSkill",
  async (newSkill) => {
    const response = await fetch("/api/skills", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newSkill),
    });
    return response.json();
  }
);

const skillsSlice = createSlice({
  name: "skills",
  initialState: { skills: [], status: "idle", error: null },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchSkills.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchSkills.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.skills = action.payload.skills;
      })
      .addCase(fetchSkills.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      })
      .addCase(postSkill.pending, (state) => {
        state.status = "loading";
      })
      .addCase(postSkill.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.skills = [...state.skills, action.payload.skill];
      })
      .addCase(postSkill.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});

export default skillsSlice.reducer;
