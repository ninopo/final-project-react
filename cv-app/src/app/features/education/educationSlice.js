import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const fetchEducation = createAsyncThunk(
  "education/fetchEducation",
  async () => {
    const response = await fetch("/api/educations");
    return response.json();
  }
);

const educationSlice = createSlice({
  name: "education",
  initialState: { educations: [], status: "idle", error: null },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchEducation.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchEducation.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.educations = action.payload.educations;
      })
      .addCase(fetchEducation.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});

export default educationSlice.reducer;
