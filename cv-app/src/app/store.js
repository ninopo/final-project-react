import { configureStore } from "@reduxjs/toolkit";
import educationReducer from "../app/features/education/educationSlice";
import skillsReducer from "../app/features/skills/skillsSlice";

const store = configureStore({
  reducer: {
    education: educationReducer,
    skills: skillsReducer,
  },
});

export default store;
