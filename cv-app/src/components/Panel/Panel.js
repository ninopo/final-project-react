import "./Panel.scss";
import PhotoBox from "../PhotoBox/PhotoBox";
import Navigation from "../Navigation/Navigation";
import Button from "../Button/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

export default function Panel({ isMinimized }) {
  return (
    <div className="panel">
      {isMinimized ? (
        <div className="icons">
          <PhotoBox avatar="/assets/images/avatar.png" />
          <Navigation isMinimized={isMinimized} />
          <Button icon={<FontAwesomeIcon icon={faChevronLeft} />} />
        </div>
      ) : (
        <div className="full-content">
          <PhotoBox
            name="John Doe"
            fontSize="small"
            avatar="/assets/images/avatar.png"
          />
          <Navigation isMinimized={isMinimized} />
          <Button
            icon={<FontAwesomeIcon icon={faChevronLeft} />}
            text="Go back"
          />
        </div>
      )}
    </div>
  );
}
