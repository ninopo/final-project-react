import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import {
  faTwitter,
  faFacebook,
  faSkype,
} from "@fortawesome/free-brands-svg-icons";
import "./Address.scss";

export default function Address() {
  return (
    <div className="contact-info">
      <a href="tel:500342242" className="contact-item social-address">
        <FontAwesomeIcon icon={faPhone} />
        500 342 242
      </a>
      <a
        href="mailto:office@kamsolutions.pl"
        className="contact-item social-address"
      >
        <FontAwesomeIcon icon={faEnvelope} />
        office@kamsolutions.pl
      </a>
      <a href="https://twitter.com/wordpress" className="contact-item">
        <FontAwesomeIcon icon={faTwitter} />
        <span className="social">
          <span>Twitter</span>
          <span className="social-address">https://twitter.com/wordpress</span>
        </span>
      </a>
      <a href="https://www.facebook.com/facebook" className="contact-item">
        <FontAwesomeIcon icon={faFacebook} />
        <span className="social">
          <span>Facebook</span>
          <span className="social-address"> https://facebook.com/facebook</span>
        </span>
      </a>
      <a href="skype:kamsolutions.pl?chat" className="contact-item">
        <FontAwesomeIcon icon={faSkype} />
        <span className="social">
          <span>Skype</span>
          <span className="social-address">kamsolutions.pl</span>
        </span>
      </a>
    </div>
  );
}
