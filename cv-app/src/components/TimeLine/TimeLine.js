import React, { useEffect } from "react";
import Info from "../Info/Info";
import { useDispatch, useSelector } from "react-redux";
import { fetchEducation } from "../../app/features/education/educationSlice";
import "./TimeLine.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSyncAlt } from "@fortawesome/free-solid-svg-icons";

export default function TimeLine() {
  const dispatch = useDispatch();
  const education = useSelector((state) => state.education);
  const educationStatus = education.status;
  const educationData = education.educations;

  useEffect(() => {
    if (educationStatus === "idle") {
      console.log("Dispatching fetchEducation");
      dispatch(fetchEducation());
    }
    console.log("Education Data:", educationData);
  }, [educationStatus, dispatch]);

  return (
    <div className="timeline-container">
      {educationStatus === "succeeded" ? (
        educationData.map((event) => (
          <div key={event.id} className="timeline-event">
            <div className="date-container">
              <div className="timeline-date">{event.date}
              </div>
              <img src="/assets/images/Rectangle 5.png"/>
            </div>
            <div className="timeline-content">
              <Info
                title={event.title}
                text={event.description}
                background="gray"
              />
            </div>
          </div>
        ))
      ) : educationStatus === "loading" ? (
        <div className="loading-container">
          <FontAwesomeIcon className="icon" icon={faSyncAlt} />
        </div>
      ) : educationStatus === "failed" ? (
        <div className="error-container">
          <p>Something went wrong; please review your server connection!</p>
        </div>
      ) : null}
    </div>
  );
}
