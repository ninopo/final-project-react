import React, { useState } from "react";
import "./Portfolio.scss";

function Portfolio() {
  const [activeTab, setActiveTab] = useState("All");

  const portfolioItems = [
    {
      tab: "All",
      url: "/assets/images/item1.jpg",
    },
    {
      tab: "UI",
      url: "/assets/images/item2.png",
    },
    {
      tab: "Code",
      url: "/assets/images/item1.jpg",
    },
    {
      tab: "UI",
      url: "/assets/images/item2.png",
    },
  ];

  return (
    <div className="portfolio">
      <div className="portfolio-tabs">
        <button
          onClick={() => setActiveTab("All")}
          className={activeTab === "All" ? "active" : ""}
        >
          All \
        </button>
        <button
          onClick={() => setActiveTab("UI")}
          className={activeTab === "UI" ? "active" : ""}
        >
          UI \
        </button>
        <button
          onClick={() => setActiveTab("Code")}
          className={activeTab === "Code" ? "active" : ""}
        >
          Code
        </button>
      </div>
      <div className="portfolio-content">
        {portfolioItems
          .filter((item) => item.tab === activeTab || activeTab === "All")
          .map((item, index) => (
            <div key={index} className="portfolio-item">
              <img src={item.url} alt={`Portfolio item ${index + 1}`} />
              <div className="portfolio-overlay">
                <h4>Some text</h4>
                <p>
                  Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
                  arcu...
                </p>
                <a href="#">View source</a>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
}

export default Portfolio;
