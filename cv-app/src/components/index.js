export { default as Feedback} from "./Feedback/Feedback";
export { default as Address} from "./Address/Address";
export { default as Panel} from "./Panel/Panel";
export { default as Expertise} from "./Expertise/Expertise";
export { default as TimeLine} from "./TimeLine/TimeLine";
export { default as Box} from "./Box/Box";
export {default as Portfolio} from "./Portfolio/Portfolio";
export {default as Skills} from "./Skills/Skills";