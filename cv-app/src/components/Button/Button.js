import "./Button.scss";
import { Link } from "react-router-dom";

export default function Button({ text, icon, link = "/" }) {
  if (icon && text) {
    return (
      <button>
        <Link to={link}>
          {icon} {text}
        </Link>
      </button>
    );
  } else if (icon) {
    return (
      <button>
        <Link to={link}>{icon}</Link>
      </button>
    );
  } else {
    return (
      <button>
        <Link to={link}>{text}</Link>
      </button>
    );
  }
}
