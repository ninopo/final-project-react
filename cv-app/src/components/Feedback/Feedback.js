import React from "react";
import "./Feedback.scss";
import Info from "../Info/Info";

export default function Feedback({ data }) {
  return (
    <div className="feedback-list">
      {data.map((item, index) => (
        <div key={index} className="feedback-item">
          <Info text={item.feedback} background="gray" />
          <div className="feedback-reporter">
            <img
              src={item.reporter.photoUrl}
              alt={item.reporter.name}
              className="reporter-photo"
            />
            <span className="reporter-name">{item.reporter.name}</span>
            <a href={item.reporter.citeUrl} className="reporter-cite">
              {new URL(item.reporter.citeUrl).hostname}
            </a>
          </div>
        </div>
      ))}
    </div>
  );
}
