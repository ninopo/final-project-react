import React from "react";
import "./Expertise.scss";

export default function Expertise({ data }) {
  return (
    <div className="expertise-container">
      {data.map((item, index) => (
        <div className="experience-item" key={index}>
          <div className="company-date">
            <span className="company">{item.info.company}</span>
            <span className="date">{item.date}</span>
          </div>
          <div className="job-description">
            <span className="job-title">{item.info.job}</span>
            <p className="job-desc">{item.info.description}</p>
          </div>
        </div>
      ))}
    </div>
  );
}
