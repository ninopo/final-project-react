import "./PhotoBox.scss";

export default function PhotoBox({
  avatar,
  name,
  fontSize,
  title,
  description,
}) {
  return (
    <div className="box">
      <div className="avatar">
        <img src={avatar} alt={name} />
      </div>
      <div className="info">
        {name && <h2 className={fontSize}>{name}</h2>}
        {title && <h3>{title}</h3>}
        {description && <p>{description}</p>}
      </div>
    </div>
  );
}
