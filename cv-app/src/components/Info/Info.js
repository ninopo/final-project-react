import "./Info.scss";

export default function Info({ title, text, background }) {
  return (
    <div className={background} id="info">
      {title && <h3>{title}</h3>}
      <p>{text}</p>
    </div>
  );
}
