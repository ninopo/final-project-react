import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faGraduationCap,
  faBriefcase,
  faFolderOpen,
  faAddressBook,
  faCommentDots,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-scroll";
import "./Navigation.scss";

export default function Navigation({ isMinimized }) {
  const [activeItem, setActiveItem] = useState("");
  const [isClickOrigin, setIsClickOrigin] = useState(false);

  const navigationItems = [
    { name: "About me", icon: faUser, target: "about-me" },
    { name: "Education", icon: faGraduationCap, target: "education" },
    { name: "Experience", icon: faBriefcase, target: "experience" },
    { name: "Portfolio", icon: faFolderOpen, target: "portfolio" },
    { name: "Contacts", icon: faAddressBook, target: "contacts" },
    { name: "Feedback", icon: faCommentDots, target: "feedbacks" },
  ];

  const handleNavigationClick = (itemName) => {
    setActiveItem(itemName);
    setIsClickOrigin(true);
    setTimeout(() => {
      setIsClickOrigin(false);
    }, 500);
  };

  return (
    <div className="navigation">
      {navigationItems.map((item) => (
        <Link
          key={item.name}
          to={item.target}
          spy={true}
          smooth={true}
          duration={500}
          className={`navigation-item ${
            item.name === activeItem ? "active" : ""
          }`}
          onClick={() => handleNavigationClick(item.name)}
          onSetActive={isClickOrigin ? null : (label) => setActiveItem(label)}
          activeClass="active"
        >
          <FontAwesomeIcon icon={item.icon} />
          {!isMinimized && item.name}
        </Link>
      ))}
    </div>
  );
}
