import "./Box.scss";

export default function Box({ title, content }) {
  return (
    <div className="container">
      <h2>{title}</h2>
      <div>{content}</div>
    </div>
  );
}
