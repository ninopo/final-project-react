import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { fetchSkills, postSkill } from "../../app/features/skills/skillsSlice";
import "./Skills.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-solid-svg-icons";

export default function Skills() {
  const dispatch = useDispatch();
  const skillsState = useSelector((state) => state.skills);
  const skillsData = skillsState.skills;
  const skillsStatus = skillsState.status;

  const [isFormOpen, setFormOpen] = useState(false);

  useEffect(() => {
    if (skillsStatus === "idle") {
      console.log("Dispatching fetchSkills");
      dispatch(fetchSkills());
    }
    console.log("Skills Data:", skillsData);
  }, [skillsStatus, dispatch]);

  const toggleForm = () => {
    setFormOpen(!isFormOpen);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      range: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("Skill name is a required field"),
      range: Yup.number()
        .required("Skill range is a required field")
        .min(10, "Skill range must be greater than or equal to 10")
        .max(100, "Skill range must be less than or equal to 100")
        .typeError("Skill range must be a 'number' type"),
    }),
    onSubmit: (values) => {
      dispatch(postSkill(values));
      formik.resetForm();
      toggleForm();
    },
  });

  return (
    <div className="skills-container">
      <button className="open-edit-btn" onClick={toggleForm}>
        <FontAwesomeIcon icon={faEdit} /> Open Edit
      </button>

      {isFormOpen && (
        <form onSubmit={formik.handleSubmit}>
          <div className="form-group">
            <label>Skill name:</label>
            <input
              type="text"
              name="name"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.name}
            />
            {formik.touched.name && formik.errors.name ? (
              <div className="error">{formik.errors.name}</div>
            ) : null}
          </div>

          <div className="form-group">
            <label>Skill range:</label>
            <input
              type="number"
              name="range"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.range}
            />
            {formik.touched.range && formik.errors.range ? (
              <div className="error">{formik.errors.range}</div>
            ) : null}
          </div>

          <button type="submit" disabled={!formik.isValid}>
            Add skill
          </button>
        </form>
      )}

      {skillsData.map((skill) => (
        <div key={skill.name} className="skill-item">
          <div className="skill-bar">
            <span className="skill-name">{skill.name}</span>
            <div
              className="skill-range"
              style={{ width: `${skill.range}%` }}
            ></div>
          </div>
        </div>
      ))}
      <div className="scale">
          <img src="/assets/images/scale.png"/>

          <div className="levels">
             <div className="basic">
               <span>Beginner</span>
               <span>Proficient</span>
             </div>
             <div className="advanced">
               <span>Expert</span>
               <span>Master</span>
             </div>
          </div>
      </div>
    </div>
  );
}
