import { createServer, Model } from "miragejs";

function makeServer() {
  createServer({
    models: {
      education: Model,
      skill: Model,
    },

    routes() {
      this.namespace = "/api";

      this.get(
        "/educations",
        (schema) => {
          console.log("Handling request for /educations");
          return schema.educations.all();
        },
        { timing: 3000 }
      );

      this.get(
        "/skills",
        (schema) => {
          return schema.skills.all();
        },
        { timing: 3000 }
      );

      this.post("/skills", (schema, request) => {
        const attrs = JSON.parse(request.requestBody);
        return schema.skills.create(attrs);
      });

      this.passthrough("/**/*.hot-update.json");
    },

    seeds(server) {
      server.create("education", {
        date: "2023",
        title: "Bachelor of Science",
        description: "Computer Science",
      });
      server.create("education", {
        date: "2022",
        title: "Masters Degree",
        description: "Machine Learning",
      });
      server.create("education", {
        date: "2021",
        title: "Maters Degree",
        description: "Mathematics",
      });
      server.create("education", {
        date: "2020",
        title: "Masters Degree",
        description: "Chemistry",
      });
      server.create("skill", { name: "HTML", range: 100 });
      server.create("skill", { name: "CSS", range: 90 });
      server.create("skill", { name: "JavaScript", range: 80 });
      server.create("skill", { name: "React.js", range: 70 });
    },
  });
}

const server = makeServer;
export default server;
